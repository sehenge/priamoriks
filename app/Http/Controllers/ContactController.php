<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contacts;
use App\Models\User;
use phpDocumentor\Reflection\Types\Integer;


class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contacts::get();

        return view('contacts')->withContacts($contacts);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function favorites(Request $request)
    {
        $id = $request->user()->id;
        $user = User::where('id', $id)->first();
        $contacts = $user->contact;

        return view('favorites')->withContacts($contacts);
    }

    public function addToFavorites(Request $request, int $id)
    {
        $user = User::where('id', $request->user()->id)->first();
        $contact = Contacts::find($id);

        $user->contact()->attach($contact);
        $user->save();

        return redirect('favorites')->with('message', 'Contact was successfully added to favorites');
    }

    public function removeFromFavorites(Request $request, int $id)
    {
        $user = User::where('id', $request->user()->id)->first();
        $contact = Contacts::find($id);

        $user->contact()->detach($contact);
        $user->save();

        return redirect('favorites')->with('message', 'Contact was successfully deleted from favorites');
    }
}
