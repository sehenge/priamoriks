<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; $i++) {
            \DB::table('user_contact')->insert(array(
                array(
                    'users_id' => 1,
                    'contacts_id' => $i
                ),
            ));
        }

    }
}
