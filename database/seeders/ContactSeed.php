<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Contacts::class, 10)->create()->each(function ($company) {
            $company->contacts()->save(factory(App\Contacts::class)->make());
        });
    }
}
